import express from 'express'
import path from 'path'
import compress from 'compression'
import { port, hostname } from '../config/env/env'
import { build } from '../config/env/paths'
import { logger } from './utils/logger'
import { redisClient } from './utils/cache'

redisClient.flush()

const app = express()
app.use(compress())

app.use('/', express.static(path.resolve(build, 'public')))

app.get('/*', (req, res, next) => {
  if (/\.(?:png|gif|jpg|jpeg|svg|ico|webp|json|js|txt|mp3)$/.test(req.path)) {
    return next()
  }
  next()
}, (req, res) => {
  try {
    const ssr = require('./middlewares/ssr')
    const { handler } = ssr
    handler(req, res)
  } catch (e) {
    logger.error(e.message)
    return res.status(503).end()
  }
})

const http = app.listen(port, err => {
  if (err) {
    logger.error(err.message)
    throw err
  }

  logger.warn(`> Ready on http://${hostname}:${port}`)
})

process.on('SIGHUP', async () => {
  http.close()
  redisClient.close()
  logger.end()
  process.exit(0)
})
