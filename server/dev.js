import express from 'express'
import webpack from 'webpack'
import hotMiddleware from 'webpack-hot-middleware'
import devMiddleware from 'webpack-dev-middleware'
import path from 'path'
import { port, hostname } from '../config/env/env'
import { root } from '../config/env/paths'
import { logger } from './utils/logger'
import { redisClient } from './utils/cache'
const client = require('../config/webpack/webpack.client')
const server = require('../config/webpack/webpack.server')

const app = express()
const compiler = webpack([client, server])

app.use(hotMiddleware(compiler, {
  log: false,
  reload: true
}))

const dev = devMiddleware(compiler, {
  publicPath: client.output.publicPath,
  watchOptions: {
    poll: true,
    ignored: /node_modules/
  },
  stats: {
    colors: true
  },
  serverSideRender: true,
  writeToDisk: true
})

app.use(dev)

dev.waitUntilValid(() => {
  redisClient.flush()
})

app.use('/static', express.static(path.resolve(root, 'static')))

app.get('/*', (req, res, next) => {
  if (/\.(?:png|gif|jpg|jpeg|svg|ico|webp|json|js|txt|mp3)$/.test(req.path)) {
    return next()
  }
  next()
}, (req, res) => {
  try {
    const ssr = require('./middlewares/ssr')
    const { handler } = ssr
    handler(req, res)
  } catch (e) {
    logger.error(e.message)
    return res.status(503).end()
  }
})

const http = app.listen(port, err => {
  if (err) {
    logger.error(err.message)
    throw err
  }

  logger.warn(`> Ready on http://${hostname}:${port}`)
})

process.on('SIGHUP', async () => {
  http.close()
  redisClient.close()
  logger.end()
  process.exit(0)
})

compiler.plugin('done', () => {
  logger.warn('Clearing /client/ module cache from server')
  Object.keys(require.cache).forEach(id => {
    if (/[/\\]client[/\\]/.test(id)) { delete require.cache[id] }
  })
})
