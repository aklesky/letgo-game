import { isProduction } from '../../config/env/env'
import { root } from '../../config/env/paths'
import fs from 'fs'
import { resolve } from 'path'
import { createLogger, format, transports } from 'winston'

const logs = resolve(root, 'logs')

if (!fs.existsSync(logs)) {
  fs.mkdirSync(logs)
}

export const logger = createLogger({
  format: format.combine(format.timestamp(), format.simple()),
  transports: [
    new transports.Stream({
      stream: fs.createWriteStream(resolve(logs, 'error.log')),
      level: 'error'
    }),
    new transports.Stream({
      stream: fs.createWriteStream(resolve(logs, 'info.log'))
    })
  ]
})

if (!isProduction) {
  logger.add(
    new transports.Console({
      format: format.combine(format.colorize(), format.simple())
    })
  )
}
