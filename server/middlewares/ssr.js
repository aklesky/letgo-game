import { ServerStyleSheet } from 'styled-components'
import { clientBundle } from '../../config/env/paths'
import { redisClient } from '../utils/cache'
import { logger } from '../utils/logger'
import fs from 'fs'
import { renderToNodeStream } from 'react-dom/server'

import { initStore } from '../../src/store'
import { getCharacterCards } from '../../src/actions/cards'

export const handler = async (req, res) => {
  if (!fs.existsSync(clientBundle)) {
    logger.error(`${clientBundle} doesn't exists.`)
    return res.status(404).end()
  }

  const cache = await redisClient.get(req.path)

  if (cache) {
    logger.info(`${req.path} => cache response`)
    return res.send(cache)
  }

  const cacheStream = redisClient.getStream(req.path)
  cacheStream.pipe(res)
  const store = initStore()
  cacheStream.write('<!DOCTYPE html><html lang="en">')

  fs.readFile(clientBundle, 'utf8', async (err, data) => {
    if (err) {
      logger.error(`${clientBundle} is not readable.`)
      return res.status(404).end()
    }

    const [head, footer] = data.split('<!-- AppRoot -->')

    const sheet = new ServerStyleSheet()
    const { staticApp } = require('../../src/entries/app.server')
    const jsx = sheet.collectStyles(staticApp(store))

    const stream = sheet.interleaveWithNodeStream(renderToNodeStream(jsx))

    cacheStream.write(head)
    await store.dispatch(getCharacterCards())

    stream.pipe(
      cacheStream,
      { end: false }
    )

    stream.on('end', () => {
      const preloadedState = store.getState()
      const preloadStateData = `
      <script>
      window.__INITIAL_STATE__ = ${JSON.stringify(preloadedState).replace(
    /</g,
    '\\u003c'
  )}
      </script>`
      const footerHtml = footer.replace('<!-- AppState-->', preloadStateData)
      cacheStream.write(footerHtml)
      cacheStream.write('</html>')
      cacheStream.end()
    })
  })
}
