const nodeExternals = require('webpack-node-externals')
const merge = require('webpack-merge')

const { isProduction } = require('../env/env')
const { distServer, nodeModules, productionServer, devServer } = require('../env/paths')

const common = require('./webpack.common')
const base = {
  name: 'server',
  devtool: !isProduction ? 'eval-source-map' : false,
  entry: {
    server: ['@babel/polyfill', isProduction ? productionServer : devServer]
  },
  externals: [nodeExternals()],
  output: {
    filename: '[name].js',
    libraryTarget: 'commonjs2',
    path: distServer
  },
  resolve: {
    modules: [nodeModules]
  },
  target: 'node'
}

module.exports = merge(common, base)
