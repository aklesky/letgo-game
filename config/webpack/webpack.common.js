
const CleanWebpackPlugin = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const webpack = require('webpack')
const { isProduction } = require('../env/env')
const { root, nodeModules, build } = require('../env/paths')
const i18n = require('../../i18n/en.json')

module.exports = {
  context: root,
  mode: isProduction ? 'production' : 'development',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  },
  resolve: {
    extensions: ['.mjs', '.js', '.jsx'],
    modules: [
      nodeModules
    ]
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: true,
        cache: true,
        terserOptions: {
          safari10: true
        },
        extractComments: {
          condition: true,
          banner: () => {
            return i18n.banner
          }
        },
        test: /\.js(\?.*)?$/i
      })
    ],
    removeAvailableModules: true,
    removeEmptyChunks: true,
    splitChunks: {
      chunks: 'async',
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/](react|react-dom|styled-components)[\\/]/,
          name: 'vendor',
          chunks: 'all',
          reuseExistingChunk: true,
          priority: 1
        },
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          name: 'modules',
          chunks: 'initial',
          reuseExistingChunk: true
        }
      }
    }
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new CleanWebpackPlugin([build], { root: root }),
    new webpack.NoEmitOnErrorsPlugin()
  ]
}
