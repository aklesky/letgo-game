const common = require('./webpack.common')
const merge = require('webpack-merge')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin')
const { isProduction } = require('../env/env')
const { template, publicEntry, publicDirectory } = require('../env/paths')
const i18n = require('../../i18n/en.json')
const { colors } = require('../../src/theme/colors')

const base = {
  name: 'client',
  target: 'web',
  devtool: !isProduction ? 'eval-source-map' : false,
  mode: isProduction ? 'production' : 'development',
  module: {
    rules: [
      {
        test: /\.(jsx)?$/,
        include: /node_modules/,
        use: ['react-hot-loader/webpack']
      }
    ]
  },
  output: {
    pathinfo: true,
    publicPath: '/',
    path: publicDirectory,
    filename: 'bundle.[hash].js',
    chunkFilename: '[name].[chunkHash].js'
  },
  node: {
    console: true,
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty'
  },
  plugins: [
    new HtmlWebPackPlugin({
      inject: true,
      title: i18n.name,
      description: i18n.description,
      template: template,
      filename: 'app.html',
      hash: false,
      cache: isProduction,
      meta: {
        viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
        'theme-color': colors.primary
      },
      favicon: false,
      minify: {
        collapseWhitespace: isProduction,
        removeRedundantAttributes: isProduction,
        useShortDoctype: isProduction,
        removeEmptyAttributes: isProduction,
        removeStyleLinkTypeAttributes: isProduction,
        keepClosingSlash: isProduction,
        minifyJS: isProduction,
        minifyCSS: isProduction,
        minifyURLs: isProduction
      }
    }),
    new ScriptExtHtmlWebpackPlugin({
      defaultAttribute: 'async'
    }),
    new webpack.HotModuleReplacementPlugin()
  ]
}

const production = {
  entry: {
    app: [ '@babel/polyfill', publicEntry ]
  }
}

const dev = {
  entry: {
    app: [ '@babel/polyfill', 'webpack-hot-middleware/client?name=client&noInfo=false&timeout=2000', publicEntry ]
  }
}

module.exports = merge(common, base, isProduction ? production : dev)
