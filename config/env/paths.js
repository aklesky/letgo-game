
const path = require('path')

const root = process.cwd()

const src = path.resolve(root, 'src')
const ssr = path.resolve(root, 'server')
const productionServer = path.resolve(ssr, 'production')
const devServer = path.resolve(ssr, 'dev')
const ssrMiddlewares = path.resolve(ssr, 'middlewares')

const nodeModules = path.resolve(root, 'node_modules')
const staticDirectory = path.resolve(root, 'public')
const template = path.resolve(staticDirectory, 'index.html')
const build = path.resolve(root, 'build')
const i18n = path.resolve(root, 'i18n')
const en = path.resolve(i18n, 'en.json')
const distServer = path.resolve(build, 'server')
const execFile = path.resolve(distServer, 'entry.js')
const publicDirectory = path.resolve(build, 'public')
const clientBundle = path.resolve(publicDirectory, 'app.html')
const entries = path.resolve(src, 'entries')
const publicEntry = path.resolve(entries, 'app.client.jsx')
const serverEntry = path.resolve(entries, 'app.server')

const requestHandler = path.resolve(ssrMiddlewares, 'ssr.js')

module.exports = {
  root,
  src,
  build,
  staticDirectory,
  i18n,
  en,
  distServer,
  execFile,
  clientBundle,
  template,
  nodeModules,
  publicEntry,
  serverEntry,
  requestHandler,
  publicDirectory,
  productionServer,
  devServer
}
