const { config } = require('dotenv')
const os = require('os')
config()

const isProduction = process.env.NODE_ENV === 'production'
const port = process.env.PORT || 4300
const hostname = process.env.NODE_APP_HOST || os.hostname()

module.exports = {
  isProduction,
  port,
  hostname
}
