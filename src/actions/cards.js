import fetch from 'isomorphic-unfetch'
import { actionCreator } from '../utils/action-creator'

export const actionTypes = {
  FETCH_CHARACTERS: '[SIMON: Characters] Get Playable characters',
  FETCH_CHARACTERS_SUCCES: '[SIMON: Characters] Loading Success.',
  FETCH_CHARACTERS_FAIL: '[SIMON: Characters] Loading failed.'
}

export const Actions = {
  loadCharacters: actionCreator(actionTypes.FETCH_CHARACTERS, 'payload')
}

export const getCharacterCards = () => {
  return async (dispatch) => {
    const pokemons = await getPokemonCharacters()
    const marvel = await getMarvelCharacters()
    dispatch(Actions.loadCharacters([...pokemons, ...marvel]))
  }
}

const getPokemonCharacters = async () => {
  const data = await fetch('https://pokeapi.co/api/v2/pokemon?limit=20')
    .then(response => response.json())
    .then(getPokemonDetails)
  return data
}

const getPokemonDetails = async (characters) => {
  if (!characters || characters.count === 0) {
    return []
  }
  const details = await characters.results.map(async (item) => {
    const character = await fetch(item.url)
      .then(response => response.json()).catch(() => false)

    if (!character) {
      return {}
    }
    return {
      type: 'pokemon',
      id: `pokemon_${character.id}`,
      name: character.name,
      image: character.sprites.front_default,
      active: false
    }
  })
  return Promise.all(details)
}

const getMarvelCharacters = async () => {
  const data = await fetch('https://gateway.marvel.com:443/v1/public/characters?apikey=18573cd3582bb0eb7903ab31e5c6d2c5&hash=e8bdd48877dbe98c40c47a7da2569fa3&ts=10&limit=20&offset=40')
    .then(response => response.json())
    .then(response => response.data)
    .then(response => response.results)
    .then(getMarvelCharacterDetails)
    .catch(() => [])
  return data
}

const getMarvelCharacterDetails = async (characters) => {
  if (!characters || characters.length === 0) {
    return []
  }
  return characters.map(character => {
    return {
      type: 'marvel',
      id: `marvel_${character.id}`,
      name: character.name,
      image: `${character.thumbnail.path}.${character.thumbnail.extension}`,
      active: false
    }
  })
}
