import sleep from '../utils/sleep'
import { actionCreator } from '../utils/action-creator'

export const actionTypes = {
  START_GAME: '[SIMON] Start new game.',
  CREATE_BOARD: '[SIMON] Create a new Board.',
  RESET_BOARD: '[SIMON]  Reset Board.',
  END_GAME: '[SIMON] Finish game.',
  PLAY_SONG: '[SIMON] Play card song.',
  STOP_SOUND: '[SIMON] Stop song.',
  VISUALIZE_CARD: '[SIMON] Visualize card',
  VISUALIZE_OFF_CARD: '[SIMON] Stop card visualization.',
  PLAYER_TURN: '[SIMON] Player turn.',
  GUESS_CARD: '[SIMON] Guess card.',
  NEW_LEVEL: '[SIMON] Player new level.'
}

export const Actions = {
  playSound: actionCreator(actionTypes.PLAY_SONG, 'id'),
  createBoard: actionCreator(actionTypes.CREATE_BOARD, 'payload'),
  resetBoard: actionCreator(actionTypes.RESET_BOARD),
  stopSound: actionCreator(actionTypes.STOP_SOUND, 'id'),
  visualize: actionCreator(actionTypes.VISUALIZE_CARD, 'id'),
  stopVisualization: actionCreator(actionTypes.VISUALIZE_OFF_CARD, 'id')
}

const next = actionCreator(actionTypes.NEW_LEVEL, 'payload')

const newGame = actionCreator(actionTypes.START_GAME, 'payload')
const guessCards = actionCreator(actionTypes.GUESS_CARD, 'payload')

const nextLevel = () => async (dispatch, getState) => {
  const { pads, game } = getState()
  const { board } = pads
  const { level } = game

  const shuffled = board.sort(() => 9 - Math.random())

  dispatch(next({ next: shuffled.slice(0, level + 1).map(item => item.id).sort(() => 0.2 - Math.random()).reverse() }))
}

const startNewGame = () => async (dispatch, getState) => {
  const { pads, game } = getState()

  const { board } = pads
  const { level } = game
  const shuffled = board.sort(() => 4 - Math.random())

  dispatch(newGame({ next: shuffled.slice(0, level + 1).map(item => item.id).sort(() => 0.3 - Math.random()).reverse()[0] }))
  const { match } = getState()
  match.positive.forEach(async item => {
    dispatch(highlight(item))
    await sleep(1500)
    dispatch(lowlight())
  })
}

const selectCards = payload => guessCards(payload)

const highlight = id => Actions.visualize(id)
const lowlight = () => Actions.stopVisualization()
const createNewBoard = payload => Actions.createBoard(payload)

const createBoard = payload => async (dispatch, getState) => {
  const { pads } = getState()
  const { characters } = pads

  const shuffled = characters.sort(() => 0.5 - Math.random())
  dispatch(createNewBoard(shuffled.slice(0, 9)))
}

const nextLevelGame = payload => async (dispatch, getState) => {
  dispatch(createBoard())
  await sleep(1000)
  dispatch(nextLevel())
}

const view = payload => async (dispatch, getState) => {
  const { match } = getState()
  for (let i = 0; i <= match.positive.length - 1; i++) {
    const id = match.positive[i]
    dispatch(highlight(id))
    await sleep(1500)
    dispatch(lowlight())
  }
}

const guess = ({ succeeded, id }) => async (dispatch, getState) => {
  dispatch(selectCards({ succeeded, id }))
  dispatch(highlight(id))
  await sleep(600)
  dispatch(lowlight())

  const { match } = getState()
  const { positive, guessed } = match
  const done = positive.length === guessed.length && succeeded

  return {
    done
  }
}

export const actionCreators = {
  startNewGame,
  nextLevelGame,
  selectCards,
  highlight,
  lowlight,
  nextLevel,
  guess,
  view,
  createBoard
}
