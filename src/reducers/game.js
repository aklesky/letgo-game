import * as fromGame from '../actions/game'

export const initialState = {
  board: [],
  viewing: true,
  isGameOver: false,
  score: 0,
  level: 0,
  highscore: 0
}

export default function game (state = initialState, action) {
  const { type, payload } = action
  switch (type) {
    case fromGame.actionTypes.START_GAME:
      return {
        ...state,
        isGameOver: false,
        score: 0,
        level: 1
      }

    case fromGame.actionTypes.VISUALIZE_OFF_CARD:
      return {
        ...state,
        viewing: false
      }

    case fromGame.actionTypes.VISUALIZE_CARD:
      return {
        ...state,
        viewing: true
      }

    case fromGame.actionTypes.NEW_LEVEL:
      return {
        ...state,
        score: state.score + 1,
        level: state.level + 1
      }

    case fromGame.actionTypes.GUESS_CARD:
      return {
        ...state,
        isGameOver: !payload.succeeded,
        highscore: (!payload.succeeded && state.score) > state.highscore ? state.score : state.highscore
      }
    default:
      return state
  }
}
