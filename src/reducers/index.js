import { combineReducers } from 'redux'

import pads from './pads'
import game from './game'
import match from './match'

const rootReducer = combineReducers({
  pads,
  game,
  match
})

export default rootReducer
