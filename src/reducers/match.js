import * as fromGame from '../actions/game'

export const initialState = {
  guessed: [],
  all: []
}

export default function blocks (state = initialState, action) {
  const { type, payload } = action
  switch (type) {
    case fromGame.actionTypes.START_GAME:
      return {
        guessed: [],
        positive: [
          payload.next
        ]
      }

    case fromGame.actionTypes.NEW_LEVEL:
      return {
        guessed: [],
        positive: [...payload.next]
      }

    case fromGame.actionTypes.GUESS_CARD:
      return {
        ...state,
        guessed: payload.succeeded ? state.guessed.concat(payload.id) : state.guessed
      }

    default:
      return state
  }
}
