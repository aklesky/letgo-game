import * as fromGame from '../actions/game'
import * as fromCards from '../actions/cards'

export const initialState = {
  board: [],
  characters: []
}

export default function pads (state = initialState, action) {
  const { type, id } = action
  switch (type) {
    case fromGame.actionTypes.VISUALIZE_CARD:
      return {
        ...state,
        board: state.board.map(b => ({
          ...b,
          active: id === b.id
        }))
      }

    case fromGame.actionTypes.VISUALIZE_OFF_CARD:
      return {
        ...state,
        board: state.board.map(b => ({
          ...b,
          active: false
        }))
      }

    case fromCards.actionTypes.FETCH_CHARACTERS:
      return {
        ...state,
        board: [],
        characters: [...state.characters, ...action.payload]
      }
    case fromGame.actionTypes.CREATE_BOARD:
      return {
        ...state,
        board: action.payload
      }
    default:
      return state
  }
}
