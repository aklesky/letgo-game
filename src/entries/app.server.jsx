
import React from 'react'
import App from '../app'
const i18n = require('../../i18n/en.json')
export const staticApp = (store) => (<App store={store} i18n={i18n} />)
