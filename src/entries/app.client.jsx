import React from 'react'
import ReactDOM from 'react-dom'
import App from '../app'
import { initStore } from '../store'
const i18n = require('../../i18n/en.json')
const store = initStore(window.__INITIAL_STATE__ || {})

ReactDOM.hydrate(<App store={store} i18n={i18n} />, document.getElementById('root'))
