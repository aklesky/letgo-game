import { createStore, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import rootReducer from './reducers'

const INITIAL_STATE = {}

export const initStore = (initialState = INITIAL_STATE) => {
  const middlewares = [thunk]

  return createStore(
    rootReducer,
    initialState,
    typeof window !== 'undefined'
      ? composeWithDevTools(applyMiddleware(...middlewares)) : compose(applyMiddleware(...middlewares))
  )
}
