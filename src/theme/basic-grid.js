import styled from 'styled-components'

export const Container = styled.section`
  margin: 0 auto;
  width: 100%;
`

export const Row = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
`

export const Col = styled.div`
  flex-basis: 20%;
  -ms-flex: auto;
  position: relative;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;


  @media(max-width: 1333px) {

    flex-basis: 25%;
  }
  @media(max-width: 850px) {

    flex-basis: 33%;
  }

  @media(max-width: 450px) {
    flex-basis: 100%;
    margin: 15px 0;
  }


`
