import { colors } from './colors'

export const theme = {
  colors,
  metrics: {
    padding: '15px'
  }
}
