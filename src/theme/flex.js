import { css } from 'styled-components'

export const flex = css`
  display: flex;
  justify-content: ${props => props.justify || 'flex-start'};
  align-items: ${props => props.align || 'flex-start'};
  flex-direction: ${props => props.row || 'column'};
`
