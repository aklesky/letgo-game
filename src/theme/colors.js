const colors = {
  primary: '#4285f3',
  secondary: '#69f0ae',
  background: '#303030',
  pink: '#9c27b0',
  white: '#fff',
  red: '#f44336'
}

module.exports = {
  colors
}
