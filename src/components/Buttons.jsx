import styled from 'styled-components'
import { elevation } from '../theme/elevation'

export const Button = styled.button`
  box-sizing: border-box;
  position: relative;
  user-select: none;
  cursor: pointer;
  outline: 0;
  border: none;
  -webkit-tap-highlight-color: transparent;
  display: inline-block;
  white-space: nowrap;
  text-decoration: none;
  vertical-align: baseline;
  text-align: center;
  margin: 0;
  min-width: 64px;
  line-height: 36px;
  padding: 0 16px;
  border-radius: 4px;
  overflow: visible;
  transform: translate3d(0,0,0);
  transition: background .4s cubic-bezier(.25,.8,.25,1),box-shadow 280ms cubic-bezier(.4,0,.2,1);

  ${elevation}
  background-color: ${props => props.theme.colors.red};
  color: ${props => props.theme.colors.white};
  width: 100%;

  &:hover {
    transform: scale(1.02);
  }
`
