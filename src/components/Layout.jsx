import styled from 'styled-components'

const Layout = styled.div`
  transition: 0.7s;
  filter: grayscale(100%);
  ${props => !props.disabled ? '' : `
    filter: none;
  `}
  background: rgba(255, 255, 255, 0.01);
  border: 0.5px solid rgba(255, 255, 255, 0.03);
  margin: 30px;
`

export default Layout
