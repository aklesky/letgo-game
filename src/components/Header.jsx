import styled from 'styled-components'
import { Row } from '../theme/basic-grid'
import { elevation } from '../theme/elevation'
export const Header = styled.header`
  background: ${props => props.theme.colors.pink};
  color: ${props => props.theme.colors.white};
  padding: ${props => props.theme.metrics.padding};
  ${elevation}
`
export const HeaderRow = styled(Row)`
  justify-content: center;
`
