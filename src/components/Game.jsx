import styled from 'styled-components'
import { Row } from '../theme/basic-grid'

const Game = styled(Row)`
  pointer-events: ${props => props.disbledPointer ? 'none' : 'initial'};
  position: relative;

`

export default Game
