import styled from 'styled-components'
import { elevation } from '../theme/elevation'
import { flex } from '../theme/flex'
export const Message = styled.div`
  border: 1px solid rgba(255, 255, 255, 0.03);
  ${elevation}
  background: rgba(0, 0, 0, 0.1);
  padding: 25px;
  color: ${props => props.theme.colors.white};

  flex: 0 1 auto;
  ${flex}
  width: 50%;
`
