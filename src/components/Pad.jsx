import styled, { css } from 'styled-components'
import { elevation } from '../theme/elevation'
const transform = `
  transform: scale(1.25);
`

const animation = css`
  from {
    border: 1px solid transparent;
    box-shadow:
      0 0 10px  transparent,
      0 0 20px  transparent,
      0 0 30px  transparent,
      0 0 40px  transparent,
      0 0 70px  transparent,
      0 0 80px  transparent,
      0 0 100px transparent,
      0 0 150px transparent;
  }
  to {
    boder: 1px solid transparent;
    box-shadow:
      0 0 5px  transparent,
      0 0 10px transparent,
      0 0 15px transparent,
      0 0 20px rgba(0, 0, 0, 0.2),
      0 0 35px rgba(0, 0, 0, 0.2),
      0 0 40px rgba(0, 0, 0, 0.2),
      0 0 50px rgba(0, 0, 0, 0.2),
      0 0 75px rgba(0, 0, 0, 0.2);
  }
`

const active = color => `
  background-color: ${color} !important
  animation: ${animation} 0.5s linear;
  ${transform}
`

const Pad = styled.div`
  position: relative;
  ::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: 0.9;
    z-index: -1;
    ${props =>
    props.pad.active
      ? `background: url(${
        props.pad.image
      }) no-repeat center center; background-size: cover;`
      : null}
    &:hover {
      background: ${props => props.theme.colors.secondary} !important;
    }
  }
  width: 150px;
  height: 150px;
  margin: 10px;
  box-sizing: border-box;
  background: ${props => props.theme.colors.secondary};
  ${elevation}
  transition: 0.2s;
  ${props => (props.pad.active ? active(props.theme.colors.red) : ``)}
  display: flex;
  cursor: pointer;

  &:active {
    &::before {
      background: url(${props => props.pad.image}) no-repeat center center;
      background-size: cover;
    }
    ${props => active(props.theme.colors.red)}
  }
`

export default Pad
