import styled from 'styled-components'
import { flex } from '../theme/flex'

export default styled.div`
  position: fixed;
  margin: auto;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 9;
  text-align: center;
  background-color: rgba(0,0,0,0.7);
  height: 100%;
  ${flex}
`
