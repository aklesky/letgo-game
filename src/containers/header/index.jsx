import React from 'react'
import { Header, HeaderRow } from '../../components/Header'
import { Col } from '../../theme/basic-grid'
import { Button } from '../../components/Buttons'

export default props => {
  const { highscore, score } = props
  return (
    <Header>
      <HeaderRow>
        <Col>{props.i18n.highscore}: {highscore}</Col>
        <Col>{props.i18n.score}: {score}</Col>
        <Col>
          <Button onClick={props.onClick}>{props.i18n.start}</Button>
        </Col>
      </HeaderRow>
    </Header>
  )
}
