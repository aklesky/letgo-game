import React from 'react'
import { Container } from '../../theme/basic-grid'
import Header from '../header'

export default props => (
  <Container>
    <Header
      highscore={props.highscore}
      score={props.score}
      onClick={props.onClick}
      i18n={props.i18n}
    />
    {props.children}
  </Container>
)
