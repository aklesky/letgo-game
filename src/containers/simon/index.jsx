import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Shell from './shell'
import { actionCreators } from '../../actions/game'
import sleep from '../../utils/sleep'
import OverlayMessage from '../overlay'
import CardPads from '../cardpads'

export class Board extends PureComponent {
  constructor (props) {
    super(props)
    this.startMatch = this.startMatch.bind(this)
    this.onPadClick = this.onPadClick.bind(this)
  }
  componentDidMount () {
    this.props.actions.createBoard()
  }

  async startMatch () {
    const { actions } = this.props
    actions.createBoard()
    await sleep(300)
    actions.startNewGame()
  }

  async onPadClick ({ id }) {
    const { actions, game, match } = this.props
    const tail = match.guessed.length
    const succeeded = match.positive[tail] === id

    if (game.isGameOver) {
      return false
    }

    const state = await actions.guess({ id, succeeded })
    if (!state.done) {
      return false
    }
    actions.nextLevel()
    await sleep(1000)
    actions.view()
  }

  onGameOver () {
    const { score, isGameOver, highscore } = this.props.game
    if (!isGameOver) {
      return
    }
    return (
      <OverlayMessage
        score={score}
        highscore={highscore}
        onClick={this.startMatch}
        i18n={this.props.i18n}
      />
    )
  }

  render () {
    const { viewing, isGameOver, score, highscore } = this.props.game
    return (
      <Shell
        score={score}
        highscore={highscore}
        onClick={this.props.actions.startNewGame}
        i18n={this.props.i18n}
      >
        {this.onGameOver()}
        <CardPads
          isGameOver={isGameOver}
          viewing={viewing}
          board={this.props.pads.board}
          guessed={this.props.match.guessed}
          onClick={this.onPadClick}
        />
      </Shell>
    )
  }
}

export default connect(
  state => state,
  dispatch => ({
    actions: bindActionCreators(actionCreators, dispatch)
  })
)(Board)
