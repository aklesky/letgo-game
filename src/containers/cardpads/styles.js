import styled from 'styled-components'
import { Row } from '../../theme/basic-grid'

export const GameRow = styled(Row)`
  max-width: 805px;
  margin: 0 auto;
  justify-content: center;
`
