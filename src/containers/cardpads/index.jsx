
import React from 'react'
import Game from '../../components/Game'
import Pad from '../../components/Pad'
import Layout from '../../components/Layout'
import { GameRow } from './styles'
import { Col } from '../../theme/basic-grid'

export default (props) => {
  const { isGameOver, viewing, board, guessed, onClick } = props
  return (
    <Layout disabled={!isGameOver}>
      <Game disbledPointer={viewing || isGameOver}>
        <GameRow>
          {board.map((pad, i) => (
            <Col key={pad.id}>
              <Pad
                pad={pad}
                active={guessed[pad.id]}
                onClick={() => onClick({ id: pad.id })}
              />
            </Col>
          ))}
        </GameRow>
      </Game>
    </Layout>
  )
}
