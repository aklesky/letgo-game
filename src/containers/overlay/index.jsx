import React from 'react'
import Overlay from '../../components/Overlay'
import { Message } from '../../components/Message'
import { Button } from '../../components/Buttons'

export default props => {
  return (
    <Overlay justify='center' align='center'>
      <Message>
        <h2>{props.i18n.score} {props.score}</h2>
        <h3>{props.i18n.highscore} {props.highscore}</h3>
        <Button onClick={props.onClick}>{props.i18n.play}</Button>
      </Message>
    </Overlay>
  )
}
