import React from 'react'
import { hot } from 'react-hot-loader/root'
import { Provider } from 'react-redux'
import { ThemeProvider } from 'styled-components'
import SinonGameContainer from './containers/simon'
import { theme } from './theme'
import GlobalStyles from './theme/globalStyles'

const app = ({ store, i18n }) => (
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <>
        <GlobalStyles />
        <SinonGameContainer i18n={i18n} />
      </>
    </Provider>
  </ThemeProvider>
)

export default hot(app)
