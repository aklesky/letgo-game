#!/bin/sh
set -e

cd /usr/src/app

dist_dir="/usr/src/app/build"

if [ ! -d "$dist_dir" ]; then
  npm run build
fi

exec "$@";
